<?php

namespace Database\Seeders;

use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Voice;
use Illuminate\Database\Seeder;

/**
 *
 */
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run(): void
    {
        Post::factory()->count(20)->has(Voice::factory()->count(10))->create();
    }
}
