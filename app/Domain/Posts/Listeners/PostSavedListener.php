<?php

namespace App\Domain\Posts\Listeners;

use App\Domain\Posts\Actions\UpdatePostInElasticAction;
use App\Domain\Posts\Events\PostSaved;

/**
 * Срабатывает при сохранения записи голоса
 */
class PostSavedListener
{
    /**
     * @param UpdatePostInElasticAction $action
     */
    public function __construct(private readonly UpdatePostInElasticAction $action)
    {
    }

    /**
     * @param PostSaved $event
     * @return void
     */
    public function handle(PostSaved $event): void
    {
        //
        $post = $event->post;
        $this->action->execute($post);
    }
}
