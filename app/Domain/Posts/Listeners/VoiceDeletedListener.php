<?php

namespace App\Domain\Posts\Listeners;

use App\Domain\Posts\Actions\ActualizePostRatingAction;
use App\Domain\Posts\Events\VoiceDeleted;

class VoiceDeletedListener
{
    /**
     * @param ActualizePostRatingAction $action
     */
    public function __construct(private readonly ActualizePostRatingAction $action)
    {
    }

    /**
     * Handle the event.
     *
     * @param \App\Domain\Posts\Events\VoiceDeleted $event
     * @return void
     */
    public function handle(VoiceDeleted $event): void
    {
        $postId = $event->voice->post_id;
        $this->action->execute($postId);
    }
}
