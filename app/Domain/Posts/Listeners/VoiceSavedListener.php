<?php

namespace App\Domain\Posts\Listeners;

use App\Domain\Posts\Actions\ActualizePostRatingAction;
use App\Domain\Posts\Events\VoiceSaved;

/**
 * Срабатывает при сохранения записи голоса
 */
class VoiceSavedListener
{
    /**
     * @param ActualizePostRatingAction $action
     */
    public function __construct(private ActualizePostRatingAction $action)
    {
    }

    /**
     * Handle the event.
     *
     * @param \App\Domain\Posts\Events\VoiceSaved $event
     * @return void
     */
    public function handle(VoiceSaved $event): void
    {
        //
        $postId = $event->voice->post_id;
        $this->action->execute($postId);
    }
}
