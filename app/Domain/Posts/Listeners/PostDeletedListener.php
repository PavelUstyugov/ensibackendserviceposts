<?php

namespace App\Domain\Posts\Listeners;

use App\Domain\Posts\Actions\DeletePostInElasticAction;
use App\Domain\Posts\Events\PostDeleted;

class PostDeletedListener
{
    /**
     * @param DeletePostInElasticAction $action
     */
    public function __construct(private readonly DeletePostInElasticAction $action)
    {
    }

    /**
     * Handle the event.
     *
     * @param \App\Domain\Posts\Events\PostDeleted $event
     * @return void
     */
    public function handle(PostDeleted $event): void
    {
        $post = $event->post;
        $this->action->execute($post);
    }
}
