<?php

namespace App\Domain\Posts\Models\Tests\Factories;

use App\Domain\Posts\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(5),
            'text_intro' => $this->faker->text(200),
            'text_full' => $this->faker->text(300),
            'user_id' => $this->faker->numberBetween(1, 100),
        ];
    }
}
