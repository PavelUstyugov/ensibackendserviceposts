<?php

namespace App\Domain\Posts\Models\Tests\Factories;

use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Voice;
use Illuminate\Database\Eloquent\Factories\Factory;

class VoiceFactory extends Factory
{
    protected $model = Voice::class;

    /**
     * варианты значений голосовв -1 и 1
     *
     * @var array|int[]
     */
    public array $variants = [-1, 1];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            //
            'user_id' => $this->faker->numberBetween(1, 100),
            'post_id' => Post::factory(),
            'voice' => $this->faker->randomElement($this->variants),
        ];
    }
}
