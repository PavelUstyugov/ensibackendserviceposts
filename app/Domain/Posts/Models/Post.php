<?php

namespace App\Domain\Posts\Models;

use App\Domain\Posts\Events\PostDeleted;
use App\Domain\Posts\Events\PostSaved;
use App\Domain\Posts\Models\Tests\Factories\PostFactory;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * Класс-модель для сущности "Запись статейной ленты".
 *
 * @property string $title - заголовок записи
 * @property string $text_intro - вводный текст для главной
 * @property string $text_full - полный текст статьи
 * @property int $user_id - идентификатор пользователя
 * @property string $slug - слаг записи
 * @property int $rating - рейтинг статьи
 * @property-read Illuminate\Support\Collection|Voice[] $voices - список записей голосов
  *
 */
class Post extends Model
{
    use HasFactory;
    use Sluggable;

    protected $fillable = [
        'title',
        'text_intro',
        'text_full',
        'user_id',
    ];

    public function sluggable(): array
    {
        return [
            'slug' => [
                'source' => 'title',
            ],
        ];
    }

    /**
     * список записей голосования для текущего поста
     *
     * @return HasMany
     */
    public function voices(): HasMany
    {
        return $this->hasMany(Voice::class);
    }

    /**
     * @return PostFactory
     */
    public static function factory(): PostFactory
    {
        return PostFactory::new();
    }

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => PostSaved::class,
        'deleted' => PostDeleted::class,
    ];
}
