<?php

namespace App\Domain\Posts\Models;

use App\Domain\Posts\Events\VoiceDeleted;
use App\Domain\Posts\Events\VoiceSaved;
use App\Domain\Posts\Models\Tests\Factories\VoiceFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

/**
 * Класс-модель для сущности "голос пользователя".
 *
  * @property int $user_id - идентификатор пользователя
 * @property int $post_id - рейтинг статьи
 * @property-read int $voice - результат голосования ползователем за данную статью
 *
 */
class Voice extends Model
{
    use HasFactory;
    use Notifiable;

    public $timestamps = false;

    protected $fillable = [
        'user_id',
        'post_id',
        'voice',
    ];

    /**
     * @return VoiceFactory
     */
    public static function factory(): VoiceFactory
    {
        return VoiceFactory::new();
    }

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => VoiceSaved::class,
        'deleted' => VoiceDeleted::class,
    ];
}
