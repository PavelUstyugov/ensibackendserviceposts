<?php

namespace App\Domain\Posts\Exceptions;

class VoiceAlreadyCreatedException extends VoiceException
{
    /**
     * @param int $user_id
     * @param int $post_id
     * @param string $title
     */
    public function __construct(int $user_id, int $post_id, string $title)
    {
        parent::__construct();
        $this->setMessage(
            "Уже был создан голос пользователем с id {$user_id} для поста " .
            "{$title} (id = {$post_id})"
        );
    }
}
