<?php

namespace App\Domain\Posts\Exceptions;

use Exception;

class VoiceException extends Exception
{
    /**
     * Задает строчку сообщения исключения в соответствии входящим значением
     *
     * @param string $message
     * @return void
     */
    public function setMessage(string $message): void
    {
        $this->message = $message;
    }
}
