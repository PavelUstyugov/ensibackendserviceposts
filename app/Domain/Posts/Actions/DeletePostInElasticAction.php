<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;

class DeletePostInElasticAction
{

    /**
     * @var Client
     */
    private Client $elasticsearch;

    public function __construct()
    {
        $this->elasticsearch = ClientBuilder::create()->setHosts(config('services.search.hosts'))
            ->build();
    }

    /**
     * @param int $id
     * @param array $fields
     * @return Post
     */
    public function execute(Post $post): void
    {
        $this->elasticsearch->delete([
            'index' => config('services.search.index'),
            'id' => $post->id,
        ]);
    }
}
