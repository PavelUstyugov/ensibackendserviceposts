<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Exceptions\VoiceAlreadyCreatedException;
use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Voice;

class CreateVoiceAction
{
    /**
     * Создает запись проголосовавшего
     *
     * @param array $fields
     * @return Voice
     */
    public function execute(array $fields): Voice
    {
        if (
            !Voice::where(
                'user_id',
                $fields['user_id']
            )->where('post_id', $fields['post_id'])
                ->exists()
        ) {
            $voice = Voice::create($fields);

            return $voice;
        }

        throw new VoiceAlreadyCreatedException(
            $fields['user_id'],
            $fields['post_id'],
            Post::find($fields['post_id'])->title
        );
    }
}
