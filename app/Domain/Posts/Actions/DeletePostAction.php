<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class DeletePostAction
{
    /**
     * Удаляет пост и связанные с ним голоса
     *
     * @param int $id
     * @return void
     * @throws \Throwable
     */
    public function execute(int $id): void
    {
        $currentPost = Post::findOrFail($id);
        $currentPost->delete();
    }
}
