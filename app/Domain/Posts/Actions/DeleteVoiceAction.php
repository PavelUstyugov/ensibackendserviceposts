<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Voice;

class DeleteVoiceAction
{
    /**
     * Удаляет голос пользователя
     *
     * @param int $id
     * @return void
     * @throws \Throwable
     */
    public function execute(int $id): void
    {
        $currentVoice = Voice::findOrFail($id);
        $currentVoice->delete();
    }
}
