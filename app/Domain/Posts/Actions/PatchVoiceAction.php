<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Voice;

class PatchVoiceAction
{
    /**
     * @param int $id
     * @param array $fields
     * @return Voice
     */
    public function execute(int $id, array $fields): Voice
    {
        $currentVoice = Voice::findOrFail($id);
        $currentVoice->update($fields);

        return $currentVoice;
    }
}
