<?php

namespace App\Domain\Posts\Actions;

use App\Domain\Posts\Models\Post;

class PatchPostAction
{
    /**
     * @param int $id
     * @param array $fields
     * @return Post
     */
    public function execute(int $id, array $fields): Post
    {
        $currentPost = Post::findOrFail($id);
        $currentPost->update($fields);

        return $currentPost;
    }
}
