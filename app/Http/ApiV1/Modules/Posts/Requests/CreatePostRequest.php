<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreatePostRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string'],
            'text_intro' => ['nullable', 'string'],
            'text_full' => ['required', 'string'],
            'user_id' => ['required', 'integer', 'between:1,100'],
        ];
    }
}
