<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchVoiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'voice' => ['required', 'integer', Rule::in([1, -1])],
        ];
    }
}
