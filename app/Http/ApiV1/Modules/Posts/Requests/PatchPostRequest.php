<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchPostRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title' => ['nullable', 'string'],
            'text_intro' => ['nullable', 'string'],
            'text_full' => ['nullable', 'string'],
            'user_id' => ['nullable', 'integer', 'between:1,100'],
        ];
    }
}
