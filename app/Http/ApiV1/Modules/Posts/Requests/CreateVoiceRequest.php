<?php

namespace App\Http\ApiV1\Modules\Posts\Requests;

use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateVoiceRequest extends BaseFormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', 'between:1,100'],
            'post_id' => ['required', 'integer', Rule::exists(Post::class, 'id')],
            'voice' => ['required', 'integer', Rule::in([1, -1])],
        ];
    }
}
