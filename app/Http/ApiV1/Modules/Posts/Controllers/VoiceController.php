<?php

namespace App\Http\ApiV1\Modules\Posts\Controllers;

use App\Domain\Posts\Actions\CreateVoiceAction;
use App\Domain\Posts\Actions\DeleteVoiceAction;
use App\Domain\Posts\Actions\PatchVoiceAction;
use App\Http\ApiV1\Modules\Posts\Queries\SearchVoiceQuery;
use App\Http\ApiV1\Modules\Posts\Requests\CreateVoiceRequest;
use App\Http\ApiV1\Modules\Posts\Requests\PatchVoiceRequest;
use App\Http\ApiV1\Modules\Posts\Resources\VoicesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class VoiceController
{
    /**
     * @param CreateVoiceRequest $request
     * @param CreateVoiceAction $action
     * @return VoicesResource
     */
    public function create(CreateVoiceRequest $request, CreateVoiceAction $action): VoicesResource
    {
        return new VoicesResource($action->execute($request->validated()));
    }

    /**
     * @param int $id
     * @param DeleteVoiceAction $action
     * @return EmptyResource
     * @throws \Throwable
     */
    public function delete(int $id, DeleteVoiceAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    /**
     * @param int $id
     * @param PatchVoiceRequest $request
     * @param PatchVoiceAction $action
     * @return VoicesResource
     */
    public function patch(int $id, PatchVoiceRequest $request, PatchVoiceAction $action): VoicesResource
    {
        return new VoicesResource($action->execute($id, $request->validated()));
    }

    /**
     * @param PageBuilderFactory $pageBuilderFactory
     * @param SearchVoiceQuery $query
     * @return AnonymousResourceCollection
     */
    public function searchVoice(PageBuilderFactory $pageBuilderFactory, SearchVoiceQuery $query): AnonymousResourceCollection
    {
        return VoicesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
