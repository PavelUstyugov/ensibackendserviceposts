<?php

namespace App\Http\ApiV1\Modules\Posts\Controllers;

use App\Domain\Posts\Actions\CreatePostAction;
use App\Domain\Posts\Actions\DeletePostAction;
use App\Domain\Posts\Actions\PatchPostAction;
use App\Http\ApiV1\Modules\Posts\Queries\PostQuery;
use App\Http\ApiV1\Modules\Posts\Requests\CreatePostRequest;
use App\Http\ApiV1\Modules\Posts\Requests\PatchPostRequest;
use App\Http\ApiV1\Modules\Posts\Resources\PostsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class PostController
{
    /**
     * @param CreatePostRequest $request
     * @param CreatePostAction $action
     * @return PostsResource
     */
    public function create(CreatePostRequest $request, CreatePostAction $action): PostsResource
    {
        return new PostsResource($action->execute($request->validated()));
    }

    /**
     * @param int $id
     * @param PostQuery $query
     * @return PostsResource
     */
    public function get(int $id, PostQuery $query): PostsResource
    {
        $result = $query->findOrFail($id);

        return new PostsResource($result);
    }

    /**
     * @param int $id
     * @param DeletePostAction $action
     * @return EmptyResource
     */
    public function delete(int $id, DeletePostAction $action): EmptyResource
    {
        $action->execute($id);

        return new EmptyResource();
    }

    /**
     * @param int $id
     * @param PatchPostRequest $request
     * @param PatchPostAction $action
     * @return PostsResource
     */
    public function patch(int $id, PatchPostRequest $request,  PatchPostAction $action): PostsResource
    {
        return new PostsResource($action->execute($id, $request->validated()));
    }

    /**
     * @param PageBuilderFactory $pageBuilderFactory
     * @param PostQuery $query
     * @return AnonymousResourceCollection
     */
    public function searchPosts(PageBuilderFactory $pageBuilderFactory, PostQuery $query): AnonymousResourceCollection
    {
        $result = PostsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );

        return $result;
    }
}
