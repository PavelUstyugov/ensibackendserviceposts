<?php

namespace App\Http\ApiV1\Modules\Posts\Queries;

use App\Domain\Posts\Models\Voice;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class SearchVoiceQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Voice::query());

        $this->allowedFilters([
            AllowedFilter::exact('user_id'),
            AllowedFilter::partial('post_id'),
        ]);
    }
}
