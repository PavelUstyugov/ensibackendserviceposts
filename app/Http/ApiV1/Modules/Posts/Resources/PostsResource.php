<?php

namespace App\Http\ApiV1\Modules\Posts\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class PostsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'text_intro' => $this->text_intro,
            'text_full' => $this->text_full,
            'user_id' => $this->user_id,
            'rating' => $this->rating,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'voices' => VoicesResource::collection($this->whenLoaded('voices')),
        ];
    }
}
