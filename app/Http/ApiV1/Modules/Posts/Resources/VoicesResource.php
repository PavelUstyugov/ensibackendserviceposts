<?php

namespace App\Http\ApiV1\Modules\Posts\Resources;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class VoicesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'post_id' => $this->post_id,
            'voice' => $this->voice,
        ];
    }
}
