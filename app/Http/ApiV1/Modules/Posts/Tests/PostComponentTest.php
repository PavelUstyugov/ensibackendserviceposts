<?php

use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Voice;
use App\Http\ApiV1\Modules\Posts\Tests\Factories\PostFactory;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/posts/posts 201 - success', function () {
    $request = PostFactory::new()->make();
    $id = postJson('/api/v1/posts/posts', $request)
        ->assertStatus(201)
        ->json('data.id');
    assertDatabaseHas((new Post())->getTable(), ['id' => $id]);
});
// не сработает потому что id пользователя в данной задаче от 1 до 100
test('POST /api/v1/posts/posts 400', function () {
    $uniqueName = uniqid();
    $request = PostFactory::new()->make(['user_id' => 101, 'title' => $uniqueName]);
    postJson('/api/v1/posts/posts', $request)
        ->assertStatus(400);

    assertDatabaseMissing((new Post())->getTable(), ['title' => $uniqueName]);
});

test('DELETE /api/v1/posts/posts/{id} 200 - success', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->has(Voice::factory()->count(5))->create();
    deleteJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(200);
    assertDatabaseMissing((new Post())->getTable(), [
        'id' => $newPost->id,
    ]);
    assertDatabaseMissing((new Voice())->getTable(), [
        'post_id' => $newPost->id,
    ]);
});

test('DELETE /api/v1/posts/posts/{id} 400', function () {
    deleteJson('/api/v1/posts/posts/{id}')
        ->assertStatus(400);
})->skip();

test('DELETE /api/v1/posts/posts/{id} 404', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->has(Voice::factory()->count(5))->create();
    deleteJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(200);
    assertDatabaseMissing((new Post())->getTable(), [
        'id' => $newPost->id,
    ]);
    deleteJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(404);
});

test('PATCH /api/v1/posts/posts/{id} 200', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->create();
    $request = PostFactory::new()->make();
    patchJson("/api/v1/posts/posts/{$newPost->id}", $request)
        ->assertStatus(200)
        ->assertJsonPath('data.title', $request['title'])
        ->assertJsonPath('data.text_intro', $request['text_intro'])
        ->assertJsonPath('data.text_full', $request['text_full'])
        ->assertJsonPath('data.user_id', $request['user_id']);
});

test('PATCH /api/v1/posts/posts/{id} 400', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->create();
    $request = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->make(['user_id' => 101]);
    $data = $request->toArray();
    patchJson("/api/v1/posts/posts/{$newPost->id}", $data)
        ->assertStatus(400);
    assertDatabaseMissing((new Post())->getTable(), ['id' => $newPost->id, 'user_id' => 101]);
});

test('PATCH /api/v1/posts/posts/{id} 404', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->has(Voice::factory()->count(5))->create();
    deleteJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(200);
    $request = PostFactory::new()->make();
    patchJson("/api/v1/posts/posts/{$newPost->id}", $request)
        ->assertStatus(404);
    assertDatabaseMissing((new Post())->getTable(), [
        'id' => $newPost->id,
    ]);
});

test('GET /api/v1/posts/posts/{id} 200', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->has(Voice::factory()->count(5))->create();
    getJson('/api/v1/posts/posts/' . $newPost->id)
        ->assertStatus(200);
});

test('GET /api/v1/posts/posts/{id} 404', function () {
    Post::count()
        ? $nonExistentId = Post::orderBy('id', 'desc')->first()->id + 1
        : $nonExistentId = 1;
    getJson('/api/v1/posts/posts/' . $nonExistentId)
        ->assertStatus(404);
});

test('POST /api/v1/posts/posts:search 200', function () {
    postJson('/api/v1/posts/posts:search')
        ->assertStatus(200);
});

test('POST /api/v1/posts/posts:search with filter 200', function () {
    Post::factory()->count(2)->create();
    $uniqueName = uniqid();
    $currentPost = Post::factory()->createOne(['title' => $uniqueName]);
    $request = [
        'filter' => ['title' => $uniqueName],
    ];
    postJson('/api/v1/posts/posts:search', $request)
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $currentPost->id);
});

test('POST /api/v1/posts/posts:search with sort 200', function () {
    Post::factory()->createOne();
    $secondPost = Post::factory()->createOne();
    $request = [
        'sort' => ['-id'],
    ];
    postJson('/api/v1/posts/posts:search', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $secondPost->id);
});

test('POST /api/v1/posts/posts:search with includes 200', function () {
    $newPost = \App\Domain\Posts\Models\Tests\Factories\PostFactory::new()->createOne();
    $voices = Voice::factory()->count(3)->create(['post_id' => $newPost->id]);
    $request = [
        'filter' => ['id' => $newPost->id],
        'include' => ['voices'],
    ];
    postJson('/api/v1/posts/posts:search', $request)
        ->assertStatus(200)
        ->assertJsonPath('data.0.id', $newPost->id)
        ->assertJsonCount($voices->count(), 'data.0.voices');
});
