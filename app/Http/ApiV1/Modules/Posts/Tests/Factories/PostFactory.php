<?php

namespace App\Http\ApiV1\Modules\Posts\Tests\Factories;

use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class PostFactory extends BaseApiFactory
{
    /**
     * @return array
     */
    protected function definition(): array
    {
        return [
            'title' => $this->faker->sentence(5),
            'text_intro' => $this->faker->text(200),
            'text_full' => $this->faker->text(300),
            'user_id' => $this->faker->numberBetween(1, 100),
        ];
    }

    /**
     * @param array $extra
     * @return array
     */
    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
