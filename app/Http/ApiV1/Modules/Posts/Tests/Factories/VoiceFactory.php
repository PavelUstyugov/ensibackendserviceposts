<?php

namespace App\Http\ApiV1\Modules\Posts\Tests\Factories;

use App\Domain\Posts\Models\Post;
use App\Http\ApiV1\Support\Tests\Factories\BaseApiFactory;

class VoiceFactory extends BaseApiFactory
{
    /**
     * варианты значений голосовв -1 и 1
     *
     * @var array|int[]
     */
    public array $variants = [-1, 1];

    /**
     * @return array
     */
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->numberBetween(1, 100),
            'post_id' => Post::factory(),
            'voice' => $this->faker->randomElement($this->variants),
        ];
    }

    /**
     * @param array $extra
     * @return array
     */
    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
