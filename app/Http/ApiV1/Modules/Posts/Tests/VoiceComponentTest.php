<?php

use App\Domain\Posts\Models\Tests\Factories\VoiceFactory;
use App\Domain\Posts\Models\Voice;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/posts/voices 201', function () {
    $request = VoiceFactory::new()->make();
    $id = postJson('/api/v1/posts/voices', $request->toArray())
        ->assertStatus(201)
        ->json('data.id');
    assertDatabaseHas((new Voice())->getTable(), ['id' => $id]);
});

test('POST /api/v1/posts/voices 400', function () {
    $user_id = 101; // impossible user
    $request = VoiceFactory::new()->make(['user_id' => $user_id]);
    postJson('/api/v1/posts/voices', $request->toArray())
        ->assertStatus(400);

    assertDatabaseMissing((new Voice())->getTable(), ['user_id' => $user_id]);
});

test('DELETE /api/v1/posts/voices/{id} 200', function () {
    $newVoice = VoiceFactory::new()->create();
    deleteJson('/api/v1/posts/voices/' . $newVoice->id)
        ->assertStatus(200);
    assertDatabaseMissing((new Voice())->getTable(), [
        'id' => $newVoice->id,
    ]);
});

test('DELETE /api/v1/posts/voices/{id} 400', function () {
    deleteJson('/api/v1/posts/voices/{id}')
        ->assertStatus(400);
})->skip();

test('DELETE /api/v1/posts/voices/{id} 404', function () {
    $newVoice = VoiceFactory::new()->create();
    deleteJson('/api/v1/posts/voices/' . $newVoice->id)
        ->assertStatus(200);
    assertDatabaseMissing((new Voice())->getTable(), [
        'id' => $newVoice->id,
    ]);
    deleteJson('/api/v1/posts/voices/' . $newVoice->id)
        ->assertStatus(404);
});

test('PATCH /api/v1/posts/voices/{id} 200', function () {
    $newVoice = VoiceFactory::new()->create();
    $request = VoiceFactory::new()->make();
    $data = $request->toArray();
    patchJson('/api/v1/posts/voices/' . $newVoice->id, $data)
        ->assertStatus(200)
        ->assertJsonPath('data.voice', $data['voice']);
});

test('PATCH /api/v1/posts/voices/{id} 400', function () {
    $newVoice = VoiceFactory::new()->create();
    $request = VoiceFactory::new()->make(['voice' => 101]);
    $data = $request->toArray();
    patchJson('/api/v1/posts/voices/'.$newVoice->id, $data)
        ->assertStatus(400);
    assertDatabaseMissing((new Voice())->getTable(), ['id' => $newVoice->id, 'user_id' => $data['user_id']]);
});

test('PATCH /api/v1/posts/voices/{id} 404', function () {
    Voice::count()
        ? $nonExistentId = Voice::orderBy('id', 'desc')->first()->id + 1
        : $nonExistentId = 1;
    $request = VoiceFactory::new()->make();
    $data = $request->toArray();
    patchJson('/api/v1/posts/voices/'. $nonExistentId, $data)
        ->assertStatus(404);
});

test('POST /api/v1/posts/voices:search 200', function () {
    postJson('/api/v1/posts/voices:search')
        ->assertStatus(200);
});
