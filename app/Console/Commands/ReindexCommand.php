<?php

namespace App\Console\Commands;

use App\Domain\Posts\Models\Post;
use Elastic\Elasticsearch\Client;
use Elastic\Elasticsearch\ClientBuilder;

use Illuminate\Console\Command;

class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexes all articles to Elasticsearch';

    /**
     * @var array
     */
    private array $hosts;

    /**
     * @var string
     */
    private string $index;

    /**
     * @var Client
     */
    private Client $elasticsearch;

    public function __construct()
    {
        parent::__construct();
        $this->hosts = config('services.search.hosts');
        $this->index = config('services.search.index');
        $this->elasticsearch = ClientBuilder::create()->setHosts($this->hosts)
            ->build();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $posts = Post::get();
        foreach ($posts as $post) {
            $this->elasticsearch->index([
                'index' => $this->index,
                'id' => $post->id,
                'body' => [
                    'title' => $post->title,
                    'text_full' => $post->text_full,
                ],
            ]);
        }
    }
}
