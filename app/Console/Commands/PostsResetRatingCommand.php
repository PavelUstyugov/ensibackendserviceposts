<?php

namespace App\Console\Commands;

use App\Domain\Posts\Models\Post;
use App\Domain\Posts\Models\Voice;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class PostsResetRatingCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'posts:reset-rating {post?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обнуляет рейтинг заданного поста. В параметрах задается id записи';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $postId = $this->argument('post');

        if (!$postId) {
            $this->error('input parameter missing');

            return 0;
        }

        DB::transaction(function () use ($postId) {
            Voice::where('post_id', $postId)->delete();

            $this->comment("user voices for post with id = {$postId} reseted");

            $post = Post::findOrFail($postId);
            $post->rating = 0;
            $post->save();

            $this->comment("rating of post with id = {$postId} reseted");
        }, 5);

        return 0;
    }
}
