<?php

namespace App\Providers;

use App\Domain\Posts\Events\PostDeleted;
use App\Domain\Posts\Events\PostSaved;
use App\Domain\Posts\Events\VoiceDeleted;
use App\Domain\Posts\Events\VoiceSaved;
use App\Domain\Posts\Listeners\PostDeletedListener;
use App\Domain\Posts\Listeners\PostSavedListener;
use App\Domain\Posts\Listeners\VoiceDeletedListener;
use App\Domain\Posts\Listeners\VoiceSavedListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        VoiceSaved::class => [VoiceSavedListener::class],
        VoiceDeleted::class => [VoiceDeletedListener::class],
        PostSaved::class => [PostSavedListener::class],
        PostDeleted::class => [PostDeletedListener::class],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
