# EnsiBackendServicePosts

Сервис Posts

## Getting started

# PIM Posts

## Резюме

Название: POSTS

Домен: Catalog

Назначение: Сервис для работы с поставки.

## Зависимости

| Название | Описание  | Переменные окружения |
|----------|---|---|
| MySQL    | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
